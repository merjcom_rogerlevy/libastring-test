IMG=astring.img
SCRS=forth.scr \
	${PREFIX}/libastring/astring.scr \
	astring-test.scr

BLKS+=${SCRS:M*.scr:R:S/$/.blk/}

.include "config.mk"

.SUFFIXES: .scr .blk
.scr.blk:
	${SCR} ${SCRFLAGS} ${.IMPSRC} ${.TARGET}

.PHONY: build clean

build: ${IMG}

clean:
	-rm -vf ${IMG}
	-rm -vf *.blk

${IMG}: forth.img.def ${BLKS}
	@echo ${BLKS}
	cp forth.img.def ${IMG}
	img -t ${IMG} 2M
	${BLKLD} ${BLKLDFLAGS} -o ${IMG} ${BLKS}
